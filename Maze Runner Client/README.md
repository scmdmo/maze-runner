Author
------
Domingo Manuel Amado Candamo


Summary
-------
The Maze application client is a console app that uses webservices Rest to communicate with Maze Server.
The application init 2 Threads named Brain and Runner. Runner get Map from the webservice.
Runner has a strategy pattern to resolve the Maze. There are 2 resolver implementations.
The cheaterResolver and the AStarResolver. CheaterResolver get the path calling, the path
webservice from the server. AStarResolver its my own custom implementation of A* Algorithm
adpated to this problem, uses distance to exit(position 0,0) as main heuristic.

Once the Brain has resolved the Maze starts to indicate one by one the positions to 
the Runner and wait for his reply. The Runner, after a move, check if he reach the exit by the webservice.
When it is reached, Runner tells the event to the Brain and finish. Runner has a delay of 2seconds for movement.

Runner and Brain are coordinate and communicate using a shared instance of CommunicationChannel.
Before write data in the CommunicationChannel they adquire a lock.
Brain is the last in finish the execution.

Observations
------------
These problem is not the most suitable to show several threads working at same time.
By the requeriments of the exercise it is using two threads work synchronized. 
Similar a Sequence program, the threads are always waiting for each other.
If we want to avoid these, we need break the limitations of the exercise statement.


Execute
-------
java -jar maze-runner-client-0.0.1-SNAPSHOT.jar
Application has enabled a dual loging. Loging output will be showed by console 
and also will be written under logs folder in maze-runner-client.log


Technology
-------
* Used Spring Boot
* Used Spring RestTemplate
* Used Spring Application context XML instead of Annotations.
* Used Log4j 
* Used Junit to test A* algorithm


Requirements
------------
* Maze Server running on local port 8080
* It requires Java 8+

Configuration
-------------
Using external configuration files are dismissed by time requeriments.
Under Resources folder we can find:
  -application-context.xml -> I recommend dont change anything in this file except:
      If you want change Mase server direction, change the urlRoot property of bean maze
            <bean id="maze" class="adapters.impl.MazeImpl">
              <property name="urlRoot" value="http://localhost:8080/rest/maze"/>
              <property name="restTemplate" ref="restTemplate"/>
            </bean>
       If you want change Brain resolver, modify the property resolver from bean task.brain:
            <bean id="task.brain" class="tasks.MazeBrainTask">
              <property name="maze" ref="maze"/>
              <property name="resolver" ref="resolver.astar"/>
              <property name="channel" ref="task.channel"/>
            </bean>
          If you use "resolver.astar" you are using the A* algorithm to find the path across the maze. DEFAULT OPTION.
          If you use "resolver.cheat" you are using the Cheatter resolver that get the Path from WebService Path.
  -log4j.properties  -> configuration of application logger.