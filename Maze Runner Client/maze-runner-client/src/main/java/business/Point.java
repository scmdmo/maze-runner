/**
 * 
 */
package business;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Inmutable object to represent a position in the Maze Map
 * @author Domin
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Point implements Serializable, Comparable<Point> {
	
	private static final long serialVersionUID = 2862581954913624812L;
	
	private final int x;
	private final int y;
	
	@JsonCreator
	public Point(@JsonProperty("x") int x, @JsonProperty("y") int y){
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public String toString() {
		return "x: "+x+" y: "+y;
	}

	@Override
	public int compareTo(Point o) {
		int v = this.x+this.y;
		int vo = o.x+o.y;
		return v-vo;
	}
}
