/**
 * 
 */
package resolver.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;

import business.Point;
import resolver.MazeResolver;

/**
 * @author Domin
 *
 */
public class AStarMazeResolver implements MazeResolver {
	private static final Logger log = Logger.getLogger(AStarMazeResolver.class);
	
	private char[][] map;
	private HashSet<Point> closePositions;
	private ArrayList<Point> openPositions;
	
	@Override
	public List<Point> resolve(char[][] map) {
		log.info("Running A* Maze resolver to find a path");
		this.map = map;
		closePositions = new HashSet<>();
		openPositions = new ArrayList<>();
		openPositions.add(new Point(map.length-1,
				map[map.length-1].length-1));
		
		while(!openPositions.isEmpty()){
			Point pos = openPositions.get(openPositions.size()-1);
			if(isEnd(pos)){
				openPositions.remove(0);//First item is the current position
				return openPositions;
			}
			List<Point>nextPoints = analizeMoves(pos);
			if(nextPoints.isEmpty()){
				openPositions.remove(openPositions.size()-1);
			}
			else{
				openPositions.add(heuristic(nextPoints));
			}
		}
		
		return null;
	}
	
	private List<Point> analizeMoves(Point p){
		log.debug("POINT :"+p);
		log.debug("xlimit :"+map[p.getY()].length);
		log.debug("ylimit :"+map.length);
		int x_limit = map[p.getY()].length;
		int y_limit = map.length;
				
		ArrayList<Point> res = new ArrayList<>(); 
		
		for(Point t_point : this.checkPoints(p)){
			boolean x_axis = t_point.getX()<x_limit && t_point.getX()>=0;
			boolean y_axis = t_point.getY()<y_limit && t_point.getY()>=0;
			if(x_axis && y_axis){
				if(map[t_point.getY()][t_point.getX()]=='O'){
					if(!closePositions.contains(t_point)){
						res.add(t_point);
					}
					closePositions.add(p);
				}
			}
		}
		return res;
	}
	
	private List<Point> checkPoints(Point p){
		ArrayList<Point> list = new ArrayList<>();
		list.add(new Point(p.getX()-1,p.getY()));
		list.add(new Point(p.getX()+1,p.getY()));
		list.add(new Point(p.getX(),p.getY()-1));
		list.add(new Point(p.getX(),p.getY()+1));
		return list;
	} 

	private Point heuristic(List<Point> points){
		Collections.sort(points);
		return points.get(0);
	}
	
	private boolean isEnd(Point p){
		return (p.getX()==0 && p.getY()==0);
	}
}
