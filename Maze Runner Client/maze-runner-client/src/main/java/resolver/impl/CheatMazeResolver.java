/**
 * 
 */
package resolver.impl;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import adapters.Maze;
import business.Point;
import resolver.MazeResolver;

/**
 * @author Domin
 *
 */
public class CheatMazeResolver implements MazeResolver {
	private static final Logger log = Logger.getLogger(CheatMazeResolver.class);
	
	private Maze maze;
	
	public void setMaze(Maze maze) {
		this.maze = maze;
	}
	
	@Override
	public List<Point> resolve(char[][] map) {
		log.info("Running Cheater Maze resolver to find a path");
		return Arrays.asList(maze.getPath());
	}
}
