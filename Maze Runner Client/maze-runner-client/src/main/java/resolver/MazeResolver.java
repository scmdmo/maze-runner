/**
 * 
 */
package resolver;

import java.util.List;

import business.Point;

/**
 * @author Domin
 *
 */
public interface MazeResolver {
	List<Point> resolve(char[][] map);
}
