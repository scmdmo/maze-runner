/**
 * 
 */
package tasks;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import business.Point;

/**
 * @author Domin
 *
 */
public class CommunicationChannel {
	
	private static final Logger log = Logger.getLogger(CommunicationChannel.class);
	
	private Lock lock;
	private Point point;
	private boolean lastPositionSuccess;
	int positionMoved;
	private boolean finish;
	
	public CommunicationChannel(){
		this.lock = new ReentrantLock();
		this.point = null;
		this.lastPositionSuccess = true;
		this.positionMoved = 0;
		this.finish = false;				
	}
	
	public Lock getLock(){
		return lock;
	}
	
	public void mazeFinished(){
		this.finish = true;
	}
	
	public boolean isFinished(){
		return finish;
	}
	
	public boolean isLastPositionSuccess() {
		return lastPositionSuccess;
	}
	
	public void setLastPositionSuccess(boolean lastPositionSuccess) {
		this.lastPositionSuccess = lastPositionSuccess;
	}
	
	public void setPoint(Point point) {
		this.point = point;
	}
	
	public Point getPoint() {
		return point;
	}
	
	public int getPositionMoved() {
		return positionMoved;
	}
	
	public void aumentPositionMoved(){
		positionMoved++;
	}
	
	public synchronized void waitForCommunication(){
		try {
			this.wait(5000);//To prevent thread death 
		} catch (InterruptedException e) {
			log.debug("waitForCommunication interrupted", e);			
		}
	}
	
	public synchronized void communicationAvailable(){
		this.notifyAll();
	}
}
