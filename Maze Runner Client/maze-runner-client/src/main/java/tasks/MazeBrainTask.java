/**
 * 
 */
package tasks;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import adapters.Maze;
import business.Point;
import resolver.MazeResolver;

/**
 * @author Domin
 *
 */
public class MazeBrainTask extends Thread {
	private static final Logger log = Logger.getLogger(MazeBrainTask.class);
	
	private Maze maze;	
	private MazeResolver resolver;
	private CommunicationChannel channel;
	private List<Point> route; 
	
	public void setMaze(Maze maze) {
		this.maze = maze;
	}
	
	public void setResolver(MazeResolver resolver) {
		this.resolver = resolver;
	}
	
	public void setChannel(CommunicationChannel channel) {
		this.channel = channel;
	}
	
	@Override
	public void run() {
		log.debug("Starting Brain Thread");
		char[][] map = maze.getMap();
		log.debug("Brain got map");
		for(char[] r :map) {
			log.debug(Arrays.toString(r));			
		}
		log.debug("tam y="+map.length+" tam x="+map[map.length-1].length);
		route = resolver.resolve(map);
		log.debug("Brain resolved route "+route.toString());
		boolean exit = false;
		while(!exit){
			log.debug("------Brain, doing his work");
			channel.getLock().lock();
			try {
				if(channel.isFinished()){
					exit = true;
					log.debug("Brain knows Runner is out. Exiting");
					continue;
				} else {
					nextPoint();
				}
			} 
			finally {
				channel.getLock().unlock();
				channel.communicationAvailable();								
			}
			log.debug("------Brain waiting for runner");
			channel.waitForCommunication();
		}
		log.debug("------Brain Finished");
	}
	
	private void nextPoint(){
		Point p = route.get(channel.getPositionMoved());
		if(channel.isLastPositionSuccess()){
			log.debug("Brain, runner advanced 1 pos. Next "+p);
		} else{
			log.debug("Brain, runner fail in the last move, sending the same position. Trying again "+p);
		}		
		channel.setPoint(p);
		channel.setLastPositionSuccess(false);
	}
}
