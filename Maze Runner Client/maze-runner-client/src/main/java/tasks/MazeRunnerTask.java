/**
 * 
 */
package tasks;

import org.apache.log4j.Logger;

import adapters.Maze;

/**
 * @author Domin
 *
 */
public class MazeRunnerTask extends Thread {
	private static final Logger log = Logger.getLogger(MazeRunnerTask.class);
	
	private Maze maze;
	private CommunicationChannel channel;
	
	public void setMaze(Maze maze) {
		this.maze = maze;
	}
	
	public void setChannel(CommunicationChannel channel) {
		this.channel = channel;
	}
	
	@Override
	public void run() {
		log.debug("Starting Runner Thread");
		boolean exit = false;
		while(!exit){
			log.debug("------Runner waiting for brain");		
			channel.waitForCommunication();
			try{
				log.debug("------Runner, doing his work");
				channel.getLock().lock();
				boolean moved = maze.moveRunner(channel.getPoint());			
				channel.setLastPositionSuccess(moved);
				if(moved){
					channel.aumentPositionMoved();
					log.debug("Runner moved to new position, "+channel.getPoint());
				} else{
					log.debug("Runner fail trying move");
				}
				channel.setPoint(null);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					log.warn("Runner interrupted", e);
				}
				if(maze.isOut()){
					channel.mazeFinished();
					exit = true;
				}
			} finally {
				channel.getLock().unlock();
				channel.communicationAvailable();
			}
		}
		log.debug("------Runner Finished");
	}
}
