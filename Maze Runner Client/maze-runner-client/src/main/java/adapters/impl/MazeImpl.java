package adapters.impl;

import org.apache.log4j.Logger;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import adapters.Maze;
import business.Point;

/**
 * @author Domin
 *
 */
public class MazeImpl implements Maze {

	private static final Logger log = Logger.getLogger(MazeImpl.class);
//	@Autowired
	private RestTemplate restTemplate;	

	private String urlRoot;
	
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public void setUrlRoot(String urlRoot) {
		this.urlRoot = urlRoot;
	}
	
	@Override	
	public char[][] getMap() {
		final String relativeUrl="/map";
		String map = restTemplate.getForObject(urlRoot+relativeUrl, String.class);
		log.debug("Getting Map from GET "+urlRoot+relativeUrl+"\n"+map);
		return readRawMap(map);
	}

	@Override
	public Boolean isOut() {
		final String relativeUrl="/isOut";
		log.debug("Calling isOut: GET "+urlRoot+relativeUrl);
		return restTemplate.getForObject(urlRoot+relativeUrl, Boolean.class);		 
	}

	@Override
	public Point[] getPath() {
		final String relativeUrl="/path";
		log.debug("Calling get Path: GET "+urlRoot+relativeUrl);
		return restTemplate.getForObject(urlRoot+relativeUrl, Point[].class);
	}

	@Override
	public Boolean moveRunner(Point p) {
		final String relativeUrl="/move";
		boolean res = false;
		try {
			restTemplate.postForObject(urlRoot+relativeUrl, p, Point.class);
			log.debug("Calling moveRunner: POST "+urlRoot+relativeUrl+" position:"+p.toString());
			res = true;
		} catch (RestClientException e){			
			log.warn("Http response status error calling POST "+urlRoot+relativeUrl, e);
//			e.printStackTrace();
			//TODO error handler to difference between connection error or business error
		}
		return res;
	}
	
	private char[][] readRawMap(String map){
		char [][] res;
		String [] rows = map.split("\n");
		res = new char[rows.length][];		
		for(int i=0; i<rows.length; i++){			
			res[i] = rows[i].replace(" ", "").toCharArray();
		}
		return res;
	}
}
