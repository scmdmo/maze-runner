package adapters;

import business.Point;

/**
 * @author Domin
 *
 */
public interface Maze {
	char[][] getMap();
	Point[] getPath();
	Boolean moveRunner(Point p);
	Boolean isOut();	
}
