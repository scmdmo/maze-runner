package app;
import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.client.RestTemplate;

import tasks.MazeBrainTask;
import tasks.MazeRunnerTask;

@SpringBootApplication
@ImportResource("classpath:application-context.xml")
@ComponentScan
public class Application {
	
	private static final Logger log = Logger.getLogger(Application.class);

	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);		
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

//  INTEGRATE TESTING DEBUG COMMANDLINE RUNNER
//	@Bean
//	public CommandLineRunner run(Maze core) throws Exception {
//		return args -> {
//			log.debug("Calling getting Path: "+Arrays.toString(core.getPath()));			
//			log.debug("Calling is Out: "+core.isOut());			
//			log.debug("Calling getting Map: ");
//			for(char[] r :core.getMap()){
//				log.debug(Arrays.toString(r));
//			}
//			Point p = new Point(6,4);
//			log.debug("Moving runner to "+p+" : "+core.moveRunner(p));
//			List<Point> path = new AStarMazeResolver().resolve(core.getMap());
//			if(path!=null){
//				log.debug("Resolving Maze: "+path.stream().map(Object::toString)
//	                    .collect(Collectors.joining(", ")));
//			} else {
//				log.debug("Resolving Maze: couldn´t find path");
//			}
//		};
//	}
	
	@Bean
	public CommandLineRunner run(MazeBrainTask brain, MazeRunnerTask runner) throws Exception {
		return args -> {
			log.info("\tRUNNING  MAZE RUNNER APP");
			runner.start();
			brain.start();
		};
	}	
}
