package resolver;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import business.Point;
import resolver.impl.AStarMazeResolver;

@RunWith(SpringRunner.class)
public class AStarMazeResolverTest {

	@Test
	public void testResolve() {
		
		char [][] map = new char[][] {
			{'O', 'O', 'O', 'X', 'O', 'O', 'X'},
			{'X', 'X', 'O', 'X', 'O', 'X', 'X'},
			{'X', 'O', 'O', 'X', 'O', 'X', 'X'},
			{'X', 'O', 'X', 'X', 'O', 'O', 'X'},
			{'X', 'O', 'O', 'O', 'O', 'X', 'X'},
			{'X', 'O', 'X', 'X', 'O', 'O', 'O'},
			{'X', 'X', 'X', 'X', 'X', 'X', 'O'}
		};
		AStarMazeResolver resolver = new AStarMazeResolver();
		List<Point> path = resolver.resolve(map);
		Assert.assertNotNull(path);
		System.out.println("Path encontrado: "+path.stream().map(Object::toString)
	                    .collect(Collectors.joining(" ; ")));
	}

}
