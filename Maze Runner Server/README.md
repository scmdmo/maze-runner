Summary
-------
The Maze application has a UI and a server component. The UI will allow you to see Runner’s progress and maze.
Server component is exposing the Rest endpoint.


Execute
-------
This will be your first challenge.
Run the Maze application and check the Api documentation.
You might find some hurdles along the way :)


URLs
----
The application will expose a Rest Api and documentation.

Api documentation - http://localhost:8282/
WADL - http://localhost:8282/rest/application.wadl
Rest model xsd - http://localhost:8282/schemas/maze.xsd


Remarks
-------
* The maze map that is loaded can be different in each run.
* If you want to start from the beginning restart the application.
* The Runner is identified by the green square.


Requirements
------------
* Server uses the port 8282
* It requires Java 8+